plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

repositories {
    mavenCentral()
}

android {
    androidExtensions {
        isExperimental = true
    }
    compileSdkVersion(Versions.Build.compiledSdk)
    defaultConfig {
        minSdkVersion(Versions.Build.minSdk)
        targetSdkVersion(Versions.Build.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile(
                "proguard-android-optimize.txt"),
                "proguard-rules.pro")
        }
    }
    testOptions {
        unitTests.isIncludeAndroidResources = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // kotlin
    implementation(Dependencies.Libs.kotlinLib)
    implementation(Dependencies.Libs.ktx)

    // network
    implementation(Dependencies.Libs.retrofit)
    implementation(Dependencies.Libs.httpLogging)

    // internal
    implementation(project(Dependencies.Internal.domain))

    // coroutines
    implementation(Dependencies.Libs.coroutines)
    testImplementation(Dependencies.Libs.coroutinesTest)

    // retrofit gson converter
    implementation(Dependencies.Libs.gson)

    // koin
    implementation(Dependencies.Libs.koin)
    testImplementation(Dependencies.Tests.koinTest)

    // tests
    testImplementation(Dependencies.Tests.junit)
    androidTestImplementation(Dependencies.Tests.runner)
    androidTestImplementation(Dependencies.Tests.espresso)
}
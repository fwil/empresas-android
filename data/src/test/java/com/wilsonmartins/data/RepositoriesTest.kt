package com.wilsonmartins.data

import com.wilsonmartins.data.network.configs.AppRetrofitManager
import com.wilsonmartins.data.network.configs.AppRetrofitManagerImpl
import com.wilsonmartins.data.network.configs.AppHttpClient
import com.wilsonmartins.data.network.configs.AppHttpClientImpl
import com.wilsonmartins.data.network.interceptors.AuthenticatorInterceptor
import com.wilsonmartins.data.network.util.ApiResultManager
import com.wilsonmartins.data.repositories.EnterpriseRepositoryImpl
import com.wilsonmartins.data.repositories.LoginRepositoryImpl
import com.wilsonmartins.data.services.ApiService
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.EnterpriseRepository
import com.wilsonmartins.domain.repositories.LoginRepository
import com.wilsonmartins.domain.usecases.enterprise.SearchEnterpriseUseCase
import com.wilsonmartins.domain.usecases.login.LoginUseCase
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.core.context.startKoin
import org.koin.dsl.module
import org.koin.test.AutoCloseKoinTest
import org.koin.test.get

val networkModuleTest = module {
    factory { AuthenticatorInterceptor() }
    single { get<AppHttpClient>().create() }
    single<AppHttpClient> { AppHttpClientImpl(get()) }
    single<AppRetrofitManager> { AppRetrofitManagerImpl(get()) }
    single<ApiService> { get<AppRetrofitManager>().retrofit().create(ApiService::class.java) }
    factory { ApiResultManager(get()) }
    single<LoginRepository> { LoginRepositoryImpl(get(), get()) }
    factory { LoginUseCase(get()) }
    single<EnterpriseRepository> { EnterpriseRepositoryImpl(get(), get()) }
    factory { SearchEnterpriseUseCase(get()) }
}

class RepositoriesTest : AutoCloseKoinTest() {

    @Before
    fun before() {
        startKoin {
            modules(networkModuleTest)
        }
    }

    @Test
    fun `test login and search`() = runBlocking {
        val loginUseCase = get<LoginUseCase>()
        val result = loginUseCase.login("testeapple@ioasys.com.br", "12341234")

        Assert.assertTrue(result is RemoteResult.Success)


        val searchUseCase = get<SearchEnterpriseUseCase>()
        val result2 = searchUseCase.searchEnterprise("AllRide")

        Assert.assertTrue(result2 is RemoteResult.Success)
    }
}
package com.wilsonmartins.data.services

import com.wilsonmartins.domain.entities.responses.EnterpriseResponse
import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.domain.entities.responses.SearchResponse
import retrofit2.Response
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Path

interface ApiService {
    @FormUrlEncoded
    @POST("users/auth/sign_in")
    suspend fun login(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<LoginResponse>

    @GET("enterprises")
    suspend fun searchEnterprise(@Query("name") name: String): Response<SearchResponse>

    @GET("enterprises/{id}")
    suspend fun enterprise(@Path("id") id: Int): Response<EnterpriseResponse>
}
package com.wilsonmartins.data.di

import com.wilsonmartins.data.network.configs.AppHttpClient
import com.wilsonmartins.data.network.configs.AppHttpClientImpl
import com.wilsonmartins.data.network.configs.AppRetrofitManager
import com.wilsonmartins.data.network.configs.AppRetrofitManagerImpl
import com.wilsonmartins.data.network.interceptors.AuthenticatorInterceptor
import com.wilsonmartins.data.network.util.ApiResultManager
import com.wilsonmartins.data.services.ApiService
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val networkModule = module {
    single { get<AppHttpClient>().create() }
    single<AppHttpClient> { AppHttpClientImpl(get()) }
    single<AppRetrofitManager> { AppRetrofitManagerImpl(get()) }
    single<ApiService> { get<AppRetrofitManager>().retrofit().create(ApiService::class.java) }

    factory { AuthenticatorInterceptor() }
    factory { ApiResultManager(get(), androidContext()) }
}
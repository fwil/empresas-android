package com.wilsonmartins.data.network.ext

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.ResponseBody.Companion.toResponseBody
import retrofit2.Response

suspend fun <T : Any> request(
    call: suspend () -> Response<T>
): Response<T> = withContext(Dispatchers.IO) {
    try {
        call.invoke()
    } catch (exception: Exception) {
        Response.error<T>(500, "{}".toResponseBody("application/json".toMediaTypeOrNull()))
    }
}

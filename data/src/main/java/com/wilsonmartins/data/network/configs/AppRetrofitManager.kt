package com.wilsonmartins.data.network.configs

import com.wilsonmartins.data.network.util.ApiUtil
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface AppRetrofitManager {
    fun retrofit(): Retrofit
}

class AppRetrofitManagerImpl(
    private val httpClient: OkHttpClient
) : AppRetrofitManager {
    override fun retrofit(): Retrofit = Retrofit.Builder().apply {
        baseUrl(ApiUtil.apiUrl())
        addConverterFactory(GsonConverterFactory.create())
        client(httpClient)
    }.build()
}

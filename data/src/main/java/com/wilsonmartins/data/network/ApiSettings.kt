package com.wilsonmartins.data.network

object ApiSettings {
    const val TIMEOUT_CONNECTION: Long = 15
    const val READ_WRITE_CONNECTION: Long = 15
    const val BASE_URL: String = "https://empresas.ioasys.com.br/api/"
    const val API_VERSION = "v1/"
    const val DEFAULT_ERROR = "Oops, we are facing some problems. Please try again."
}
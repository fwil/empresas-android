package com.wilsonmartins.data.network.util

import com.wilsonmartins.data.network.ApiSettings

object ApiUtil {
    fun apiUrl(): String = "${ApiSettings.BASE_URL}${ApiSettings.API_VERSION}"
}
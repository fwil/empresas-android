package com.wilsonmartins.data.network.util

import com.wilsonmartins.data.network.configs.AppRetrofitManager
import com.wilsonmartins.domain.entities.responses.generic.RemoteError
import okhttp3.ResponseBody
import retrofit2.Converter

class ApiErrorConversor(private val retrofitManager: AppRetrofitManager) {

    fun getConvertedError(responseBody: ResponseBody?): RemoteError? {
        return try {
            val conversor: Converter<ResponseBody, RemoteError> = retrofitManager
                .retrofit()
                .responseBodyConverter(RemoteError::class.java, arrayOfNulls<Annotation>(0))
            conversor.convert(responseBody!!)
        } catch (_: Exception) { null }
    }
}

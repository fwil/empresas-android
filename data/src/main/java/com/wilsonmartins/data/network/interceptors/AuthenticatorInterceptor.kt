package com.wilsonmartins.data.network.interceptors

import com.wilsonmartins.data.entities.Session
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthenticatorInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        var newRequest: Request? = null
        Session.credentials?.let {
            newRequest = original.newBuilder().apply {
                header("access-token", it.accessToken)
                header("client", it.client)
                header("uid", it.uid)
            }.build()
        }

        return newRequest?.let {
            chain.proceed(it)
        } ?: run {
            chain.proceed(original)
        }
    }
}
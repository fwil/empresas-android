package com.wilsonmartins.data.network.util

import android.content.Context
import com.wilsonmartins.data.network.configs.AppRetrofitManager
import com.wilsonmartins.data.network.ApiSettings
import com.wilsonmartins.data.util.Authenticator
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import retrofit2.Response
import java.io.IOException

class ApiResultManager(
    private val retrofitManager: AppRetrofitManager,
    private val context: Context
) {
    fun <T : Any> result(response: Response<T>): RemoteResult<T> {
        return if (response.isSuccessful && response.body() != null) {
            RemoteResult.Success(response.body()!!)
        } else if (response.code() == 401) {
            Authenticator.closeUserSession(context)
            return RemoteResult.Logout
        } else {
            val errorConversor = ApiErrorConversor(retrofitManager)
            errorConversor.getConvertedError(response.errorBody())?.let {
                RemoteResult.Failure(it)
            } ?: run {
                RemoteResult.InternalError(IOException(ApiSettings.DEFAULT_ERROR))
            }
        }
    }
}
package com.wilsonmartins.data.network.configs

import com.wilsonmartins.data.network.ApiSettings
import com.wilsonmartins.data.network.interceptors.AuthenticatorInterceptor
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

interface AppHttpClient {
    fun create(): OkHttpClient
}

class AppHttpClientImpl(
    private val authenticatorInterceptor: AuthenticatorInterceptor
) : AppHttpClient {

    override fun create(): OkHttpClient = OkHttpClient.Builder().apply {
        connectTimeout(ApiSettings.TIMEOUT_CONNECTION, TimeUnit.SECONDS)
        readTimeout(ApiSettings.READ_WRITE_CONNECTION, TimeUnit.SECONDS)
        writeTimeout(ApiSettings.READ_WRITE_CONNECTION, TimeUnit.SECONDS)
        addInterceptor(authenticatorInterceptor)
        addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
    }.build()
}

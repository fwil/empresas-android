package com.wilsonmartins.data.util

import android.content.Context
import com.wilsonmartins.data.entities.PreferencesConstants
import com.wilsonmartins.data.entities.Session
import com.wilsonmartins.data.entities.UserCredentials
import retrofit2.Response

object Authenticator {
    fun <T : Any> registerLoginIfNecessary(response: Response<T>, context: Context) {
        if (response.isSuccessful) {
            val headers = response.headers()
            val accessToken: String = headers["access-token"] ?: ""
            val client: String = headers["client"] ?: ""
            val uid: String = headers["uid"] ?: ""
            if (accessToken.isNotEmpty() &&
                client.isNotEmpty() &&
                uid.isNotEmpty()) {
                val credentials = UserCredentials(
                    accessToken = accessToken,
                    client = client,
                    uid = uid
                )
                saveCredentials(credentials, context)
                Session.credentials = credentials
            }
        }
    }

    fun saveCredentials(userCredentials: UserCredentials, context: Context) {
        val sharedPreferences =
            context.getSharedPreferences(
                PreferencesConstants.sharePreferencesKey, PreferencesConstants.mode)
        sharedPreferences.edit().apply {
            putString(PreferencesConstants.token, userCredentials.accessToken)
            putString(PreferencesConstants.client, userCredentials.client)
            putString(PreferencesConstants.uid, userCredentials.uid)
        }.apply()
    }

    fun addSession(token: String, client: String, uid: String) {
        val credentials = UserCredentials(
            accessToken = token,
            client = client,
            uid = uid
        )
        Session.credentials = credentials
    }

    fun closeUserSession(context: Context) {
        Session.credentials = null
        val sharedPreferences =
            context.getSharedPreferences(
                PreferencesConstants.sharePreferencesKey, PreferencesConstants.mode)
        sharedPreferences.edit().apply {
            putString(PreferencesConstants.token, null)
            putString(PreferencesConstants.client, null)
            putString(PreferencesConstants.uid, null)
        }.apply()
    }
}
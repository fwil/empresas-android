package com.wilsonmartins.data.repositories

import com.wilsonmartins.data.network.ext.request
import com.wilsonmartins.data.network.util.ApiResultManager
import com.wilsonmartins.data.services.ApiService
import com.wilsonmartins.domain.entities.responses.EnterpriseResponse
import com.wilsonmartins.domain.entities.responses.SearchResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.EnterpriseRepository

class EnterpriseRepositoryImpl(
    private val apiService: ApiService,
    private val resultManager: ApiResultManager
) : EnterpriseRepository {
    override suspend fun searchEnterprise(name: String): RemoteResult<SearchResponse> {
        val response = request { apiService.searchEnterprise(name) }
        return resultManager.result(response)
    }

    override suspend fun enterprise(id: Int): RemoteResult<EnterpriseResponse> {
        val response = apiService.enterprise(id)
        return resultManager.result(response)
    }

}
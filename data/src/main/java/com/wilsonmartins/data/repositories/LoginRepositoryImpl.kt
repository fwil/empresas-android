package com.wilsonmartins.data.repositories

import com.wilsonmartins.data.network.ext.request
import com.wilsonmartins.data.network.util.ApiResultManager
import com.wilsonmartins.data.services.ApiService
import com.wilsonmartins.data.util.Authenticator
import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.LoginRepository
import android.content.Context.MODE_PRIVATE
import android.content.Context
import com.wilsonmartins.data.entities.PreferencesConstants


class LoginRepositoryImpl(
    private val apiService: ApiService,
    private val apiResultManager: ApiResultManager,
    private val context: Context
) : LoginRepository {
    override fun isUserLogged(): Boolean {
        val sharedPreferences =
            context.getSharedPreferences(PreferencesConstants.sharePreferencesKey, MODE_PRIVATE)

        val token = sharedPreferences.getString(PreferencesConstants.token, null)
        token?.let {
            val client = sharedPreferences.getString(PreferencesConstants.client, "")
            val uid = sharedPreferences.getString(PreferencesConstants.uid, "")

            Authenticator.addSession(it, client!!, uid!!)
            return true
        }
        return false
    }

    override suspend fun login(email: String, password: String): RemoteResult<LoginResponse> {
        val response = request { apiService.login(email, password) }
        Authenticator.registerLoginIfNecessary(response, context)
        return apiResultManager.result(response)
    }
}
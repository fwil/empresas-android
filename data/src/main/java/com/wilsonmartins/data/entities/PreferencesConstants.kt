package com.wilsonmartins.data.entities

import android.content.Context

object PreferencesConstants {
    const val sharePreferencesKey = "authenticator_preferences"
    const val token = "token"
    const val uid = "uid"
    const val client = "client"
    const val mode = Context.MODE_PRIVATE
}
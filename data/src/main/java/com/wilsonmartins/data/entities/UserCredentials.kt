package com.wilsonmartins.data.entities

data class UserCredentials(
    val accessToken: String,
    val client: String,
    val uid: String
)
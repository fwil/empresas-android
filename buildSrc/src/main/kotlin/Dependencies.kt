object Dependencies {

    object Build {
        const val androidGradlePlugin = "com.android.tools.build:gradle:${Versions.Build.buildToolsVersion}"
        const val kotlinGradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.Build.kotlinGradlePlugin}"
    }

    object Libs {
        // kotlin
        const val kotlinLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.Libs.kotlin}"
        const val ktx = "androidx.core:core-ktx:${Versions.Libs.appCompat}"

        // ui
        const val appCompat = "androidx.appcompat:appcompat:${Versions.Libs.appCompat}"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.Libs.constraintLayout}"
        const val material = "com.google.android.material:material:${Versions.Libs.material}"

        // retrofit
        const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.Libs.retrofit}"

        // http httpLogging
        const val httpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.Libs.httpLogging}"

        // coroutines
        const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.Libs.coroutines}"
        const val coroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.Libs.coroutines}"

        // gson
        const val gson = "com.squareup.retrofit2:converter-gson:${Versions.Libs.gson}"

        // koin
        const val koin = "org.koin:koin-androidx-viewmodel:${Versions.Libs.koin}"

        // glide
        const val glide = "com.github.bumptech.glide:glide:${Versions.Libs.glide}"
        const val glideProcessor = "com.github.bumptech.glide:compiler:${Versions.Libs.glide}"

        // lifecycle
        const val lifecycleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.Libs.lifecycle}"
        const val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.Libs.lifecycle}"
        const val lifecycleCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.Libs.lifecycle}"
    }

    object Internal {
        const val domain = ":domain"
        const val data = ":data"
    }

    object Tests {
        const val junit = "junit:junit:${Versions.Tests.junit}"
        const val runner = "androidx.test:runner:${Versions.Tests.runner}"
        const val espresso = "androidx.test.espresso:espresso-core:${Versions.Tests.espresso}"
        const val koinTest = "org.koin:koin-test:${Versions.Libs.koin}"
    }
}
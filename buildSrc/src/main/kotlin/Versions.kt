object Versions {
    object Build {
        const val compiledSdk = 29
        const val minSdk = 19
        const val targetSdk = 29
        const val buildToolsVersion = "3.5.1"
        const val kotlinGradlePlugin = "1.3.50"
    }

    object Libs {
        const val kotlin = "1.3.50"
        const val appCompat = "1.1.0"
        const val constraintLayout = "1.1.3"
        const val material = "1.0.0"
        const val koin = "2.0.1"
        const val retrofit = "2.6.2"
        const val lifecycle = "2.2.0-alpha05"
        const val coroutines = "1.3.2"
        const val httpLogging = "4.2.1"
        const val gson = "2.6.1"
        const val glide = "4.10.0"
    }

    object Tests {
        const val junit = "4.12"
        const val runner = "1.2.0"
        const val espresso = "3.2.0"
    }
}
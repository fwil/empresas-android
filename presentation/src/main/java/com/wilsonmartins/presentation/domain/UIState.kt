package com.wilsonmartins.presentation.domain

sealed class UIState<out T : Any> {
    data class Content<out T : Any>(val content: T) : UIState<T>()
    data class Error(val error: String) : UIState<Nothing>()
    object Loading : UIState<Nothing>()
}
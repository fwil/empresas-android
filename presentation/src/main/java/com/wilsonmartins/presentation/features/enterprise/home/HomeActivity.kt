package com.wilsonmartins.presentation.features.enterprise.home

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.wilsonmartins.domain.entities.Enterprise
import com.wilsonmartins.presentation.R
import com.wilsonmartins.presentation.domain.UIState
import com.wilsonmartins.presentation.features.common.AppStrings
import com.wilsonmartins.presentation.features.enterprise.details.DetailsActivity
import com.wilsonmartins.presentation.features.enterprise.home.adapters.SearchEnterpriseAdapter
import com.wilsonmartins.presentation.features.login.LoginActivity
import com.wilsonmartins.presentation.utils.hide
import com.wilsonmartins.presentation.utils.show
import kotlinx.android.synthetic.main.activity_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class HomeActivity : AppCompatActivity() {

    private val homeViewModel: HomeViewModel by viewModel()
    private lateinit var adapter: SearchEnterpriseAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setSupportActionBar(toolbar)

        setupObservers()
        setupSearchRecyclerView()
    }

    private fun setupSearchRecyclerView() {
        adapter = SearchEnterpriseAdapter(this) {
            DetailsActivity.open(this@HomeActivity, it)
        }
        enterprisesRecyclerView.adapter = adapter
    }

    private fun setupObservers() {
         homeViewModel.uiState.observe(this, Observer {
             when (it) {
                 is UIState.Content -> onSuccess(it.content.enterprises)
                 is UIState.Loading -> onLoading()
                 is UIState.Error -> onError(it.error)
             }
         })

        homeViewModel.logoutUser.observe(this, Observer {
            if (it) {
                Toast.makeText(
                    this, getString(R.string.login_expired), Toast.LENGTH_SHORT).show()
                LoginActivity.open(this, fromLogout = true)
            }
        })
    }

    private fun onError(error: String) {
        textView.text = error
        textView.show()
        searchProgressBar.hide()
        enterprisesRecyclerView.hide()
    }

    private fun onLoading() {
        textView.hide()
        enterprisesRecyclerView.hide()
        searchProgressBar.show()
    }

    private fun onSuccess(enterprises: List<Enterprise>) {
        if (enterprises.isEmpty()) {
            onError(AppStrings.SEARCH_ERROR)
        } else {
            enterprisesRecyclerView.show()
            searchProgressBar.hide()
            textView.hide()
            adapter.setResults(enterprises)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_home, menu)

        val searchMenu: MenuItem = menu.findItem(R.id.action_search)
        val searchView: SearchView = searchMenu.actionView as SearchView
        searchView.setOnCloseListener {
            textView.text = getString(R.string.home_start_app_text)
            navLogoImageView.show()
            false
        }
        searchView.setOnSearchClickListener {
            navLogoImageView.hide()
        }
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    homeViewModel.searchEnterprise(newText)
                }
                return false
            }
        })

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the HomeActivity/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun open(parent: AppCompatActivity) {
            val intent = Intent(parent, HomeActivity::class.java)
            parent.startActivity(intent)
        }
    }
}

package com.wilsonmartins.presentation.features.common

object AppStrings {
    const val LOGIN_ERROR = "Não foi possível realizar o login. Tente novamente."
    const val SEARCH_ERROR = "Infelizmente não encotramos nada :("
}
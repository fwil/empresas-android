package com.wilsonmartins.presentation.features.enterprise.home.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wilsonmartins.domain.entities.Enterprise
import com.wilsonmartins.presentation.R
import com.wilsonmartins.presentation.utils.setImageRemote
import kotlinx.android.synthetic.main.item_search_enterprise.view.*

class SearchEnterpriseAdapter(
    private val context: Context,
    private val action: (enterprise: Enterprise) -> Unit
) : RecyclerView.Adapter<SearchEnterpriseAdapter.ViewHolder>() {

    private var enterprisesFounded: List<Enterprise> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(
            R.layout.item_search_enterprise, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = enterprisesFounded.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val enterprise = enterprisesFounded[position]
        holder.bind(enterprise)
        holder.itemView.setOnClickListener { action(enterprise) }
    }

    fun setResults(enterprises: List<Enterprise>) {
        enterprisesFounded = enterprises
        this.notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val titleTextView = itemView.titleTextView
        private val subtitleTextView = itemView.subtitleTextView
        private val countryTextView = itemView.countryTextView
        private val imageView = itemView.enterpriseImageView

        fun bind(enterprise: Enterprise) {
            titleTextView.text = enterprise.enterpriseName
            subtitleTextView.text = enterprise.enterpriseType.enterpriseTypeName
            countryTextView.text = enterprise.country

            enterprise.photo?.let {
                imageView.setImageRemote(it)
            }
        }
    }
}
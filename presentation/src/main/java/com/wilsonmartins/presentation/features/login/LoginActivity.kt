package com.wilsonmartins.presentation.features.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.presentation.R
import com.wilsonmartins.presentation.domain.UIState
import com.wilsonmartins.presentation.features.enterprise.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupObservers()
        setupActions()
    }

    private fun setupActions() {
        loginButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            loginViewModel.loginUser(email, password)
        }
    }

    private fun setupObservers() {
        loginViewModel.checkUserSession()
        loginViewModel.uiState.observe(this, Observer {
            when (it) {
                is UIState.Content -> onContent(it.content)
                is UIState.Error -> onError(it.error)
                is UIState.Loading -> onLoading()
            }
        })

        loginViewModel.logado.observe(this, Observer {
            if (it) {
                HomeActivity.open(this)
            }
        })
    }

    private fun onContent(content: LoginResponse) {
        manageFields(true)
        loginButton.text = getString(R.string.login_action_button)

        HomeActivity.open(this)
    }

    private fun onError(error: String) {
        manageFields(true)
        loginButton.text = getString(R.string.login_action_button)

        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun onLoading() {
        manageFields(false)

        loginButton.text = getString(R.string.loading)
    }

    private fun manageFields(enabled: Boolean) {
        loginButton.isEnabled = enabled
        emailEditText.isEnabled = enabled
        passwordEditText.isEnabled = enabled
    }

    companion object {
        fun open(parent: AppCompatActivity, fromLogout: Boolean = false) {
            val intent = Intent(parent, LoginActivity::class.java)
            if (fromLogout) {
                intent.addFlags(
                    Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            }
            parent.startActivity(intent)
        }
    }
}

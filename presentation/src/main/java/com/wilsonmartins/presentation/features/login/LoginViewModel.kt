package com.wilsonmartins.presentation.features.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.usecases.login.LoginStatusUseCase
import com.wilsonmartins.domain.usecases.login.LoginUseCase
import com.wilsonmartins.presentation.domain.UIState
import com.wilsonmartins.presentation.features.common.AppStrings
import kotlinx.coroutines.launch

class LoginViewModel(
    private val loginUseCase: LoginUseCase,
    private val loginStatusUseCase: LoginStatusUseCase
) : ViewModel() {

    val uiState: MutableLiveData<UIState<LoginResponse>> = MutableLiveData()

    val logado: MutableLiveData<Boolean> = MutableLiveData()

    fun loginUser(email: String, password: String) {
        uiState.value = UIState.Loading
        viewModelScope.launch {
            when (val resultLogin = loginUseCase.login(email, password)) {
                is RemoteResult.Success -> {
                    uiState.value = UIState.Content(resultLogin.result)
                }
                else -> {
                    uiState.value = UIState.Error(AppStrings.LOGIN_ERROR)
                }
            }
        }
    }

    fun checkUserSession() {
        logado.value = loginStatusUseCase.hasSession()
    }
}
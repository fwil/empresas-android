package com.wilsonmartins.presentation.features.enterprise.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wilsonmartins.domain.entities.responses.SearchResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.usecases.enterprise.SearchEnterpriseUseCase
import com.wilsonmartins.presentation.domain.UIState
import com.wilsonmartins.presentation.features.common.AppStrings
import kotlinx.coroutines.launch

class HomeViewModel(
    private val searchUseCase: SearchEnterpriseUseCase
) : ViewModel() {

    val uiState: MutableLiveData<UIState<SearchResponse>> = MutableLiveData()

    val logoutUser: MutableLiveData<Boolean> = MutableLiveData()

    fun searchEnterprise(newText: String) {
        if (newText.isNotEmpty()) {
            uiState.value = UIState.Loading
            viewModelScope.launch {
                when (val newResults = searchUseCase.searchEnterprise(newText)) {
                    is RemoteResult.Success -> {
                        uiState.value = UIState.Content(newResults.result)
                    }
                    is RemoteResult.Logout -> {
                        logoutUser.value = true
                    } else -> {
                        uiState.value = UIState.Error(AppStrings.SEARCH_ERROR)
                    }
                }
            }
        }
    }
}
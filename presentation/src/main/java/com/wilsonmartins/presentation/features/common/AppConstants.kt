package com.wilsonmartins.presentation.features.common

object AppConstants {
    const val BASE_IMAGE_URL = "http://empresas.ioasys.com.br"
    const val ENTERPRISE_KEY = "ENTERPRISE"
}
package com.wilsonmartins.presentation.features.enterprise.details

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.wilsonmartins.domain.entities.Enterprise
import com.wilsonmartins.presentation.R
import com.wilsonmartins.presentation.features.common.AppConstants.ENTERPRISE_KEY
import com.wilsonmartins.presentation.utils.setImageRemote

import kotlinx.android.synthetic.main.activity_details.*

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setSupportActionBar(toolbar)

        setupActivityConfigs()
        setupEnterpriseData()
    }

    private fun setupEnterpriseData() {
        val enterprise = intent.getSerializableExtra(ENTERPRISE_KEY) as Enterprise

        supportActionBar?.title = enterprise.enterpriseName.toUpperCase()
        enterpriseDescriptionTextView.text = enterprise.description

        enterprise.photo?.let {
            enterpriseHeaderImageView.setImageRemote(it)
        }
    }

    private fun setupActivityConfigs() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    companion object {
        fun open(parent: AppCompatActivity, enterprise: Enterprise) {
            val intent = Intent(parent, DetailsActivity::class.java).apply {
                putExtra(ENTERPRISE_KEY, enterprise)
            }
            parent.startActivity(intent)
        }
    }
}

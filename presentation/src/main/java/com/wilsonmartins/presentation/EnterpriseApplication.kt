package com.wilsonmartins.presentation

import android.app.Application
import com.wilsonmartins.presentation.di.allModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class EnterpriseApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@EnterpriseApplication)
            modules(allModules)
        }
    }
}
package com.wilsonmartins.presentation.utils

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.wilsonmartins.presentation.features.common.AppConstants

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun ImageView.setImageRemote(path: String) {
    val url = AppConstants.BASE_IMAGE_URL + path
    val requestOptions = RequestOptions()
        .diskCacheStrategy(DiskCacheStrategy.ALL)

    Glide.with(context).load(url).apply(requestOptions).into(this)
}
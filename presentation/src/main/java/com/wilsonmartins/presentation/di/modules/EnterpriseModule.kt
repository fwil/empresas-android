package com.wilsonmartins.presentation.di.modules

import com.wilsonmartins.data.repositories.EnterpriseRepositoryImpl
import com.wilsonmartins.domain.repositories.EnterpriseRepository
import com.wilsonmartins.domain.usecases.enterprise.SearchEnterpriseUseCase
import com.wilsonmartins.presentation.features.enterprise.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val enterpriseModule = module {
    // repositories
    single<EnterpriseRepository> { EnterpriseRepositoryImpl(get(), get()) }

    // use cases
    factory { SearchEnterpriseUseCase(get()) }

    // view models
    viewModel { HomeViewModel(get()) }
}
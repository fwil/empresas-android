package com.wilsonmartins.presentation.di

import com.wilsonmartins.data.di.networkModule
import com.wilsonmartins.presentation.di.modules.enterpriseModule
import com.wilsonmartins.presentation.di.modules.loginModule

val allModules = listOf(
    networkModule,
    loginModule,
    enterpriseModule
)
package com.wilsonmartins.presentation.di.modules

import com.wilsonmartins.data.repositories.LoginRepositoryImpl
import com.wilsonmartins.domain.repositories.LoginRepository
import com.wilsonmartins.domain.usecases.login.LoginStatusUseCase
import com.wilsonmartins.domain.usecases.login.LoginUseCase
import com.wilsonmartins.presentation.features.login.LoginViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val loginModule = module {
    // repositories
    single<LoginRepository> { LoginRepositoryImpl(get(), get(), androidContext()) }

    // use cases
    factory { LoginUseCase(get()) }
    factory { LoginStatusUseCase(get()) }

    // view model
    viewModel { LoginViewModel(get(), get()) }
}
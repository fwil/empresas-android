plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("android.extensions")
    kotlin("kapt")
}

android {
    androidExtensions {
        isExperimental = true
    }
    compileSdkVersion(Versions.Build.compiledSdk)
    defaultConfig {
        applicationId = "com.wilsonmartins.presentation"
        minSdkVersion(Versions.Build.minSdk)
        targetSdkVersion(Versions.Build.targetSdk)
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile(
                    "proguard-android-optimize.txt"),
                    "proguard-rules.pro")
        }
    }
    testOptions {
        unitTests.isIncludeAndroidResources = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // kotlin
    implementation(Dependencies.Libs.kotlinLib)
    implementation(Dependencies.Libs.ktx)

    // appcompat
    implementation(Dependencies.Libs.appCompat)
    implementation(Dependencies.Libs.constraintLayout)
    implementation(Dependencies.Libs.material)

    // lifecycle
    implementation(Dependencies.Libs.lifecycle)
    implementation(Dependencies.Libs.lifecycleViewModel)
    kapt(Dependencies.Libs.lifecycleCompiler)

    // coroutines
    implementation(Dependencies.Libs.coroutines)
    testImplementation(Dependencies.Libs.coroutinesTest)

    // koin
    implementation(Dependencies.Libs.koin)
    testImplementation(Dependencies.Tests.koinTest)

    // glide
    implementation(Dependencies.Libs.glide)
    kapt(Dependencies.Libs.glideProcessor)

    // tests
    testImplementation(Dependencies.Tests.junit)
    androidTestImplementation(Dependencies.Tests.runner)
    androidTestImplementation(Dependencies.Tests.espresso)

    // internal
    implementation(project(Dependencies.Internal.data))
    implementation(project(Dependencies.Internal.domain))
}

package com.wilsonmartins.domain.repositories

import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult

interface LoginRepository {
    suspend fun login(email: String, password: String): RemoteResult<LoginResponse>

    fun isUserLogged(): Boolean
}
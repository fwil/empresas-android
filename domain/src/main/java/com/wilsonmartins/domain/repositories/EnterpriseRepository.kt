package com.wilsonmartins.domain.repositories

import com.wilsonmartins.domain.entities.responses.EnterpriseResponse
import com.wilsonmartins.domain.entities.responses.SearchResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult

interface EnterpriseRepository {
    suspend fun searchEnterprise(name: String): RemoteResult<SearchResponse>

    suspend fun enterprise(id: Int): RemoteResult<EnterpriseResponse>
}
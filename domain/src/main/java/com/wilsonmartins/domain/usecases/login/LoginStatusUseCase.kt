package com.wilsonmartins.domain.usecases.login

import com.wilsonmartins.domain.repositories.LoginRepository

class LoginStatusUseCase(private val repository: LoginRepository) {
    fun hasSession(): Boolean = repository.isUserLogged()
}
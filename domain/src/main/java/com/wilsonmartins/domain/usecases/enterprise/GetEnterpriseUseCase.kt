package com.wilsonmartins.domain.usecases.enterprise

import com.wilsonmartins.domain.entities.responses.EnterpriseResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.EnterpriseRepository

class GetEnterpriseUseCase(private val repository: EnterpriseRepository) {
    suspend fun getEnterprise(id: Int): RemoteResult<EnterpriseResponse> = repository.enterprise(id)
}
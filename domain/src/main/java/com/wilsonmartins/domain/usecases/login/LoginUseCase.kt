package com.wilsonmartins.domain.usecases.login

import com.wilsonmartins.domain.entities.responses.LoginResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.LoginRepository


class LoginUseCase(private val repository: LoginRepository) {
    suspend fun login(email: String, password: String): RemoteResult<LoginResponse> = repository.login(email, password)
}
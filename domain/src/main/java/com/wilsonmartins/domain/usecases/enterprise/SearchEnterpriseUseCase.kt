package com.wilsonmartins.domain.usecases.enterprise

import com.wilsonmartins.domain.entities.responses.SearchResponse
import com.wilsonmartins.domain.entities.responses.generic.RemoteResult
import com.wilsonmartins.domain.repositories.EnterpriseRepository

class SearchEnterpriseUseCase(private val repository: EnterpriseRepository) {
    suspend fun searchEnterprise(name: String): RemoteResult<SearchResponse> = repository.searchEnterprise(name)
}
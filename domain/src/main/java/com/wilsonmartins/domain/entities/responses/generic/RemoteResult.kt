package com.wilsonmartins.domain.entities.responses.generic

sealed class RemoteResult<out T : Any> {
    data class Success<out T : Any>(val result: T) : RemoteResult<T>()
    data class Failure(val error: RemoteError) : RemoteResult<Nothing>()
    data class InternalError(val exception: Exception) : RemoteResult<Nothing>()
    object Logout: RemoteResult<Nothing>()
}
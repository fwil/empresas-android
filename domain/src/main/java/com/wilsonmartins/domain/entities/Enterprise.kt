package com.wilsonmartins.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Enterprise(
    @SerializedName("id") val id: Int,
    @SerializedName("enterprise_name") val enterpriseName: String,
    @SerializedName("description") val description: String,
    @SerializedName("email_enterprise") val email_enterprise: String? = null,
    @SerializedName("facebook") val facebook: String? = null,
    @SerializedName("twitter") val twitter: String? = null,
    @SerializedName("linkedin") val linkedin: String? = null,
    @SerializedName("phone") val phone: String? = null,
    @SerializedName("own_enterprise") val ownEnterprise: Boolean,
    @SerializedName("photo") val photo: String? = null,
    @SerializedName("value") val value: Double,
    @SerializedName("shares") val shares: Int,
    @SerializedName("share_price") val sharePrice: Double,
    @SerializedName("own_shares") val ownShares: Int,
    @SerializedName("city") val city: String,
    @SerializedName("country") val country: String,
    @SerializedName("enterprise_type") val enterpriseType: EnterpriseType
): Serializable
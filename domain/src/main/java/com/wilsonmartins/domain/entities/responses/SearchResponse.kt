package com.wilsonmartins.domain.entities.responses

import com.google.gson.annotations.SerializedName
import com.wilsonmartins.domain.entities.Enterprise

data class SearchResponse(
    @SerializedName("enterprises") val enterprises: List<Enterprise>
)
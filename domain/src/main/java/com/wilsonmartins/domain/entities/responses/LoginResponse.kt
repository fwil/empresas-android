package com.wilsonmartins.domain.entities.responses

import com.wilsonmartins.domain.entities.Enterprise
import com.wilsonmartins.domain.entities.Investor

data class LoginResponse(
    val enterprise: Enterprise? = null,
    val success: Boolean,
    val investor: Investor
)
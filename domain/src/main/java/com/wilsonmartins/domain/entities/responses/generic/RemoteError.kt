package com.wilsonmartins.domain.entities.responses.generic

import com.google.gson.annotations.SerializedName

data class RemoteError(
    @SerializedName("status") val status: String,
    @SerializedName("error") val error: String
)
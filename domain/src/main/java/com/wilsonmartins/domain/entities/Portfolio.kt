package com.wilsonmartins.domain.entities

import com.google.gson.annotations.SerializedName

data class Portfolio(
    @SerializedName("enterprises_number") val enterprisesNumber: Int,
    @SerializedName("enterprises") val enterprises: List<Any>
)
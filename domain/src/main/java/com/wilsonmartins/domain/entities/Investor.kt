package com.wilsonmartins.domain.entities

import com.google.gson.annotations.SerializedName

data class Investor(
    @SerializedName("id") val id: Int,
    @SerializedName("investor_name") val investorName: String,
    @SerializedName("email") val email: String,
    @SerializedName("city") val city: String,
    @SerializedName("country") val country: String,
    @SerializedName("balance") val balance: Double,
    @SerializedName("photo") val photo: String? = null,
    @SerializedName("portfolio") val portfolio: Portfolio,
    @SerializedName("portfolio_value") val portfolioValue: Double,
    @SerializedName("first_access") val first_access: Boolean,
    @SerializedName("super_angel") val super_angel: Boolean
)
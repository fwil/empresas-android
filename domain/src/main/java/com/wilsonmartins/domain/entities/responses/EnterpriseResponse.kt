package com.wilsonmartins.domain.entities.responses

import com.wilsonmartins.domain.entities.Enterprise

data class EnterpriseResponse(
    val enterprise: Enterprise,
    val success: Boolean
)
package com.wilsonmartins.domain.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EnterpriseType(
    @SerializedName("id") val id: Int,
    @SerializedName("enterprise_type_name") val enterpriseTypeName: String
): Serializable